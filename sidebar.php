<?php
defined( 'ABSPATH' ) OR exit( 'No direct script access allowed' );
/**
 * Pindina main sidebar.
 *
 * @package 	WordPress
 * @subpackage 	Pindina Theme
 * @category 	Theme Templates
 * @author 		Kader Bouyakoub <bkader@mail.com>
 * @link 		https://github.com/bkader
 * @copyright 	Copyright (c) 2018, Kader Bouyakoub (https://github.com/bkader)
 * @since 		Version 1.0.0
 * @version 	1.0.0
 */
?>
<aside class="sidebar" id="sidebar" role="complementary">
	<?php
	/**
	 * First we check if there is an ad put on this area and
	 * the "wp_adposts" function exists to display the ad.
	 */
	if ( function_exists( 'wp_adposts' ) ) {
		echo wp_adposts( 'sidebar' );
	}

	// If there are any widgets, display theme.
	if ( is_active_sidebar( 'sidebar' ) ) {
		dynamic_sidebar( 'sidebar' );
	}
	?>
</aside><!--/.sidebar-->
